GOPATH ?= $(HOME)/go
PATH := $(GOPATH)/bin:$(PATH)
PKGPATH ?= gitlab.com/redfield/toolstack
DESTDIR ?= /

.PHONY: all
all: bins

.PHONY: bins
bins:
	mkdir -p bin
	go build -o bin/netctl-client cmd/netctl-client/*.go
	go build -o bin/netctl-service cmd/netctl-service/*.go
	go build -o bin/netctl cmd/netctl/*.go

.PHONY: install
install:
	install -d -m 0755 $(DESTDIR)/etc/dbus-1/system.d/
	install -m 0644 configs/com.gitlab.redfield.api.netctl.conf $(DESTDIR)/etc/dbus-1/system.d/

.PHONY: clean
clean:
	rm -f management/test/mock_management.go
	rm -rf bin/

.PHONY: deps
deps:
	go get ./...


.PHONY: fmt
fmt:
	find api/ cmd/ pkg/ -pname '*.go' | xargs gofmt -w -s

.PHONY: vendor
vendor:
	GO111MODULE=on go mod vendor

api/management/management.pb.go: api/management/management.proto
	protoc -I api/management --go_out=plugins=grpc:api/management api/management/management.proto

.PHONY: proto
proto: api/management/management.pb.go

.PHONY: check
check: goreportcard all test
	DESTDIR=/tmp make install

.PHONY: test
test:
	@echo "We really should have some tests :D"

.PHONY: goreportcard
goreportcard: gofmt govet gocyclo golint ineffassign misspell

.PHONY: gofmt
gofmt:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gofmt

.PHONY: govet
govet:
	go tool vet api/ cmd/ pkg/

.PHONY: gocyclo
gocyclo:
	gometalinter --deadline=90s --disable-all --vendor ./... -E gocyclo --cyclo-over 15

.PHONY: golint
golint:
	gometalinter --deadline=90s --disable-all --vendor ./... -E golint --min-confidence=0.85

.PHONY: ineffassign
ineffassign:
	gometalinter --deadline=90s --disable-all --vendor ./... -E ineffassign

.PHONY: misspell
misspell:
	gometalinter --deadline=90s --disable-all --vendor ./... -E misspell
