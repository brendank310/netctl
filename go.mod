module gitlab.com/redfield/netctl

require (
	github.com/godbus/dbus v4.1.0+incompatible
	github.com/golang/protobuf v1.2.0
	github.com/jcelliott/lumber v0.0.0-20160324203708-dd349441af25 // indirect
	github.com/nanobox-io/golang-scribble v0.0.0-20180621225840-336beac0a992
	github.com/prometheus/procfs v0.0.0-20190209105433-f8d8b3f739bd
	github.com/rs/xid v1.2.1
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	google.golang.org/grpc v1.18.0
)
