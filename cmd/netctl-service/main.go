// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"log"
	"net"

	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api/netctl"
	"gitlab.com/redfield/netctl/pkg/netctl/service"
)

func main() {
	lis, err := net.Listen("tcp", ":50051")
	if err != nil {
		log.Fatalf("Failed to listen: %v", err)
	}
	log.Print("Starting netctl server...")

	srvr := service.NewServer()
	g := grpc.NewServer()

	netctl.RegisterNetCtlServiceServer(g, srvr)

	if err := srvr.Start(); err != nil {
		log.Fatalln(err)
	}

	g.Serve(lis)
}
