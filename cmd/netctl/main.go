// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"fmt"
	"log"

	pb "gitlab.com/redfield/netctl/api/netctl"
	"golang.org/x/net/context"
	"google.golang.org/grpc"
)

var (
	// Command line flags
	fIfname = flag.String("i", "", "Name of network interface")
	fUUID   = flag.String("n", "", "UUID of target client")

	fSSID          = flag.String("ssid", "", "SSID of the target network")
	fPSK           = flag.String("psk", "", "PSK used in WPA-PSK authentication")
	fKeyMgmt       = flag.String("key-mgmt", "", "Key management for the target network")
	fIdentity      = flag.String("identity", "", "Identity used in EAP authentication")
	fPassword      = flag.String("password", "", "Password used ni EAP authentication")
	fCACert        = flag.String("cacert", "", "Path to CA cert")
	fClientCert    = flag.String("cert", "", "Path to client cert")
	fPrivKey       = flag.String("key", "", "Path to private key")
	fPrivKeyPasswd = flag.String("keypasswd", "", "Password for private key")
	fEAP           = flag.String("eap-flags", "", "EAP flags")

	fConnect    = flag.Bool("connect", false, "Connect to network")
	fDisconnect = flag.Bool("disconnect", false, "Disconnect from the current network")

	fWatch   = flag.Bool("watch", false, "Watch the updates from netctl client")
	fVerbose = flag.Bool("v", false, "More verbose information")

	fServerAddr = flag.String("server-url", "localhost:50051", "hostname:port")
)

func connectWireless(client pb.NetCtlServiceClient, uuid string, ifname string, conf pb.WirelessNetworkConf) {
	nc := pb.NetCtlClient{Uuid: uuid}
	iface := pb.NetworkInterface{Name: ifname}

	cmd := pb.NetCtlCommand{
		Client:   &nc,
		Type:     pb.NetCtlCommand_CONNECT,
		Iface:    &iface,
		WifiConf: &conf,
	}
	r := pb.WirelessCommandRequest{Cmd: &cmd}

	_, _ = client.WirelessCommand(context.Background(), &r)
}

func disconnectWireless(client pb.NetCtlServiceClient, uuid string, ifname string) {
	nc := pb.NetCtlClient{Uuid: uuid}
	iface := pb.NetworkInterface{Name: ifname}

	cmd := pb.NetCtlCommand{
		Client: &nc,
		Type:   pb.NetCtlCommand_DISCONNECT,
		Iface:  &iface,
	}
	r := pb.WirelessCommandRequest{Cmd: &cmd}

	_, _ = client.WirelessCommand(context.Background(), &r)
}

func watch(client pb.NetCtlServiceClient, uuid string, verbose bool) {
	nc := pb.NetCtlClient{Uuid: uuid}
	r := pb.MonitorStatusRequest{Client: &nc}

	stream, err := client.MonitorStatus(context.Background(), &r)
	if err != nil {
		fmt.Print(err)
		return
	}
	for {
		reply, err := stream.Recv()
		if err != nil {
			fmt.Print(err)
			return
		}
		update := reply.GetUpdate()

		props := update.GetProps()
		uuid := update.GetClient().GetUuid()

		switch update.GetType() {
		case pb.StatusUpdate_STATE:
			state := props.GetState().String()
			ifname := props.GetIface().GetName()

			if ssid := props.GetWifiConf().GetSsid(); ssid != "" {
				log.Printf("Client %v: Iface %v: SSID %v: State changed: %v", uuid, ifname, ssid, state)
			} else {
				log.Printf("Client %v: Iface %v: State changed: %v", uuid, ifname, state)
			}

		case pb.StatusUpdate_IP:
			ip := props.GetIface().GetIpAddress()
			ifname := props.GetIface().GetName()
			log.Printf("Client %v: Iface %v: IP changed: %v\n", uuid, ifname, ip)

		case pb.StatusUpdate_SIGNAL:
			signal := props.GetIface().GetSignalStrength()
			ifname := props.GetIface().GetName()
			log.Printf("Client %v: Iface %v: Signal changed: %v\n", uuid, ifname, signal)

		case pb.StatusUpdate_ACCESSPOINTS:
			if !verbose {
				break
			}
			networks := props.GetIface().GetAccessPoints()
			ifname := props.GetIface().GetName()

			log.Printf("Client %v: Iface %v: Available networks\n", uuid, ifname)
			for _, v := range networks {
				ssid := v.GetSsid()
				signal := v.GetSignalStrength()
				keymgmt := v.GetKeyMgmt()
				log.Printf("SSID: %v\tSignal: %v\tKeyMgmt: %v\n", ssid, signal, keymgmt)
			}

		default:
			log.Print("Received unknown update type: ", update.GetType())
		}
	}
}

func main() {
	flag.Parse()

	conn, err := grpc.Dial(*fServerAddr, grpc.WithInsecure())
	if err != nil {
		fmt.Println(err)
		return
	}
	defer conn.Close()

	client := pb.NewNetCtlServiceClient(conn)

	// netctl client uuid is required
	var uuid string
	if *fUUID == "" {
		fmt.Println("Error: must specify a client by UUID")
		return
	}
	uuid = *fUUID

	if *fWatch {
		watch(client, uuid, *fVerbose)
	}

	if *fDisconnect {
		if *fIfname == "" {
			fmt.Print("Error: Must specify a network interface for connection")
			return
		}
		disconnectWireless(client, uuid, *fIfname)
	}

	// Setup network configuration
	if *fConnect {
		if *fIfname == "" {
			fmt.Print("Error: Must specify a network interface for connection")
			return
		}
		conf := pb.WirelessNetworkConf{
			Ssid:          *fSSID,
			Psk:           *fPSK,
			KeyMgmt:       *fKeyMgmt,
			Identity:      *fIdentity,
			Password:      *fPassword,
			Eap:           *fEAP,
			CaCert:        *fCACert,
			ClientCert:    *fClientCert,
			PrivKey:       *fPrivKey,
			PrivKeyPasswd: *fPrivKeyPasswd,
		}
		connectWireless(client, uuid, *fIfname, conf)
	}
}
