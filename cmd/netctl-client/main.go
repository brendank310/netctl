// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"flag"
	"log"
	"os"
	"os/signal"
	"syscall"

	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/pkg/netctl/client"
	"gitlab.com/redfield/netctl/pkg/netctl/supplicant"
)

var (
	serverAddr = flag.String("server-url", "localhost:50051", "hostname:port")
)

func main() {
	flag.Parse()

	conn, err := grpc.Dial(*serverAddr, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("Could not establish connection: %v", err)
	}
	defer conn.Close()

	// Find active wireless interfaces
	names, err := supplicant.WirelessInterfaceNames()
	if err != nil {
		log.Fatalf("unable to find wireless interfaces: %v", err)
	}

	// Start a netctl client for each interface found
	for _, name := range names {
		opts := []client.ClientOption{
			client.WithPeriodicScan(),
			client.WithRememberNetworks(),
			client.WithConnectLastNetwork(),
		}

		c, err := client.NewClient(conn, name, opts...)
		if err != nil {
			log.Printf("could not start netctl client for interface %v: %v", name, err)
		}

		c.Register()
	}

	// Run until interrupted
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// Wait for signal
	<-c

	os.Exit(1)
}
