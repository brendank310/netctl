// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"log"
	"os"
	"os/signal"
	"syscall"

	"github.com/godbus/dbus"
	"github.com/golang/protobuf/jsonpb"
	"golang.org/x/net/context"

	"gitlab.com/redfield/netctl/api/netctl"
)

const (
	netctlDBusObjectPath = "/com/gitlab/redfield/api/netctl"

	netctlDBusDestination  = "com.gitlab.redfield.api.netctl"
	netctlDBusUpdateSignal = "com.gitlab.redfield.api.netctl.UpdateSignal"
)

// Send status updates to the UI
func (s *Server) updateSignalEmitter(uuid string) {
	// Get a system bus to communicate with UI
	bus, err := dbus.SystemBus()
	if err != nil {
		log.Print(err)
		return
	}

	// Register a new update listener
	broadcaster := s.updateBroadcasters[uuid]
	listener := broadcaster.registerListener()
	defer broadcaster.unregisterListener(listener)

	for {
		update := <-listener.update

		m := jsonpb.Marshaler{
			EnumsAsInts:  true,
			EmitDefaults: true,
			Indent:       "\t",
			OrigName:     true,
			AnyResolver:  nil,
		}

		s, err := m.MarshalToString(&update)
		if err != nil {
			log.Print("Failed to marshal pb msg to string", err)
			continue
		}
		bus.Emit(netctlDBusObjectPath, netctlDBusUpdateSignal, s)
	}
}

type netctlDBusInterface struct {
	serv *Server
}

func (ndi *netctlDBusInterface) NetCtlCommand(cmd string) *dbus.Error {
	msg := netctl.NetCtlCommand{}
	err := jsonpb.UnmarshalString(cmd, &msg)
	if err != nil {
		log.Printf("Error unmarshaling command: %v", err)
		return dbus.MakeFailedError(err)
	}

	r := netctl.WirelessCommandRequest{Cmd: &msg}
	_, err = ndi.serv.WirelessCommand(context.Background(), &r)
	if err != nil {
		log.Printf("Failed to send netctl command: %v", err)
		return dbus.MakeFailedError(err)
	}

	return nil
}

func startDBusAdapter(s *Server) {
	ndi := netctlDBusInterface{serv: s}

	bus, err := dbus.SystemBus()
	if err != nil {
		log.Print(err)
		return
	}

	reply, err := bus.RequestName(netctlDBusDestination, dbus.NameFlagDoNotQueue)
	if err != nil {
		log.Print(err)
		return
	}

	// Make sure we own the name
	if reply != dbus.RequestNameReplyPrimaryOwner {
		log.Print("DBus name is already taken...")
		return
	}

	err = bus.Export(&ndi, netctlDBusObjectPath, netctlDBusDestination)
	if err != nil {
		log.Print(err)
		return
	}

	// Run until interrupted
	c := make(chan os.Signal)
	signal.Notify(c, os.Interrupt, syscall.SIGTERM)

	// Wait for signal
	<-c

	// Cleanup
	log.Print("Releasing DBus session name")
	bus.ReleaseName(netctlDBusDestination)

	os.Exit(1)
}
