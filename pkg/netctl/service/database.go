// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"encoding/json"
	"fmt"

	scribble "github.com/nanobox-io/golang-scribble"

	"gitlab.com/redfield/netctl/api/netctl"
)

const (
	netctlServiceDirectory  = "/storage/services/netctl"
	netctlNetworksDirectory = "/storage/services/netctl/networks"
)

type database struct {
	// Driver for network configurations database
	networks *scribble.Driver
}

func createDatabase() (*database, error) {
	// Create networks database
	networks, err := scribble.New(netctlNetworksDirectory, nil)
	if err != nil {
		return nil, fmt.Errorf("could not create database for netctl: %v", err)
	}

	db := &database{
		networks: networks,
	}

	return db, nil
}

func (db *database) saveNetwork(uuid string, network *netctl.WirelessNetworkConf) {
	name := network.GetSsid()

	db.networks.Write(uuid, name, network)
}

func (db *database) getSavedNetworks(uuid string) ([]*netctl.WirelessNetworkConf, error) {
	var networks []*netctl.WirelessNetworkConf

	records, err := db.networks.ReadAll(uuid)
	if err != nil {
		return networks, fmt.Errorf("could not retrieve saved networks: %v", err)
	}

	for _, r := range records {
		var network netctl.WirelessNetworkConf
		if err := json.Unmarshal([]byte(r), &network); err != nil {
			return networks, fmt.Errorf("could not retrieve saved networks: %v", err)
		}

		networks = append(networks, &network)
	}

	return networks, nil
}
