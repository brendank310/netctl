// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"sync"

	"github.com/rs/xid"

	"gitlab.com/redfield/netctl/api/netctl"
)

type broadcaster struct {
	update chan netctl.StatusUpdate // Main update channel

	mux       sync.Mutex
	listeners map[string]chan netctl.StatusUpdate // Listener channels, e.g. UI
}

type listener struct {
	update chan netctl.StatusUpdate
	id     string
}

func (b *broadcaster) registerListener() *listener {
	// Store the new listener channel
	id := xid.New().String()
	uc := make(chan netctl.StatusUpdate)

	b.mux.Lock()
	b.listeners[id] = uc
	b.mux.Unlock()

	lis := listener{
		update: uc,
		id:     id,
	}

	return &lis
}

func (b *broadcaster) unregisterListener(lis *listener) {
	// Remove the listener
	b.mux.Lock()
	delete(b.listeners, lis.id)
	b.mux.Unlock()
}

func (b *broadcaster) broadcastUpdates() {
	for {
		update := <-b.update

		// Broadcast update to listeners
		b.mux.Lock()
		for _, v := range b.listeners {
			v <- update
		}
		b.mux.Unlock()
	}
}

func newBroadcaster() *broadcaster {
	b := broadcaster{
		update:    make(chan netctl.StatusUpdate, 100),
		listeners: make(map[string]chan netctl.StatusUpdate),
	}

	return &b
}
