// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package service

import (
	"errors"
	"fmt"
	"log"

	"golang.org/x/net/context"

	"gitlab.com/redfield/netctl/api/netctl"
)

// Server implements a netctl server.
type Server struct {
	// update broadcasters, keys are client uuid
	updateBroadcasters map[string]*broadcaster
	// Channels used to communicate cmds
	// The key values are uuid
	cmdChannels map[string]chan netctl.NetCtlCommand

	// Database for the server
	*database
}

// NewServer returns a new Server.
func NewServer() *Server {
	srvr := Server{
		updateBroadcasters: make(map[string]*broadcaster),
		cmdChannels:        make(map[string]chan netctl.NetCtlCommand),
	}

	return &srvr
}

// RegisterClient registers a netctl client with the server.
func (s *Server) RegisterClient(ctx context.Context, r *netctl.RegisterClientRequest) (*netctl.RegisterClientReply, error) {
	uuid := r.GetClient().GetUuid()

	// Get an update broadcaster and cmd channel for the client
	s.updateBroadcasters[uuid] = newBroadcaster()
	s.cmdChannels[uuid] = make(chan netctl.NetCtlCommand, 100)

	// Start update broadcaster
	go s.updateBroadcasters[uuid].broadcastUpdates()

	// Start the update monitor for the UI
	go s.updateSignalEmitter(uuid)

	log.Print("Registered netctl client with uuid: ", uuid)

	return &netctl.RegisterClientReply{}, nil
}

// SaveNetwork caches a network from the client.
func (s *Server) SaveNetwork(ctx context.Context, r *netctl.SaveNetworkRequest) (*netctl.SaveNetworkReply, error) {
	uuid := r.GetClient().GetUuid()

	s.saveNetwork(uuid, r.GetNetwork())

	return &netctl.SaveNetworkReply{}, nil
}

// GetSavedNetworks returns a list of cached networks.
func (s *Server) GetSavedNetworks(ctx context.Context, r *netctl.GetSavedNetworksRequest) (*netctl.GetSavedNetworksReply, error) {
	uuid := r.GetClient().GetUuid()

	networks, err := s.getSavedNetworks(uuid)
	if err != nil {
		return nil, err
	}

	return &netctl.GetSavedNetworksReply{Networks: networks}, nil
}

// MonitorStatus returns a stream of status updates sent by a client.
func (s *Server) MonitorStatus(r *netctl.MonitorStatusRequest, stream netctl.NetCtlService_MonitorStatusServer) error {
	broadcaster, ok := s.updateBroadcasters[r.GetClient().GetUuid()]
	if !ok {
		return errors.New("target client is not registered with netctl-service")
	}

	// Register a new update listener
	listener := broadcaster.registerListener()
	defer broadcaster.unregisterListener(listener)

	for {
		update := <-listener.update

		r := netctl.MonitorStatusReply{Update: &update}
		if err := stream.Send(&r); err != nil {
			log.Print(err)
			return err
		}
	}

	// Unreachable
}

// UpdateStatus updates the status information of a client.
func (s *Server) UpdateStatus(ctx context.Context, r *netctl.UpdateStatusRequest) (*netctl.UpdateStatusReply, error) {
	uuid := r.GetUpdate().GetClient().GetUuid()

	broadcaster, ok := s.updateBroadcasters[uuid]
	if !ok {
		err := errors.New("target client is not registered with netctl-service")
		return &netctl.UpdateStatusReply{}, err
	}

	broadcaster.update <- *r.GetUpdate()

	return &netctl.UpdateStatusReply{}, nil
}

// MonitorCommands returns a stream of commands from the server to a client.
func (s *Server) MonitorCommands(r *netctl.MonitorCommandsRequest, stream netctl.NetCtlService_MonitorCommandsServer) error {
	c, ok := s.cmdChannels[r.GetClient().GetUuid()]
	if !ok {
		return errors.New("target client is not registered with netctl-service")
	}

	for {
		cmd := <-c
		r := netctl.MonitorCommandsReply{Cmd: &cmd}

		if err := stream.Send(&r); err != nil {
			log.Print(err)
			return err
		}
	}

	// Unreachable
}

// WirelessCommand sends a command, e.g. Connect,Disconnect, to a client.
func (s *Server) WirelessCommand(ctx context.Context, r *netctl.WirelessCommandRequest) (*netctl.WirelessCommandReply, error) {
	uuid := r.GetCmd().GetClient().GetUuid()

	c, ok := s.cmdChannels[uuid]
	if !ok {
		err := errors.New("target client is not registered with netctl-service")
		return &netctl.WirelessCommandReply{}, err
	}

	c <- *r.GetCmd()

	return &netctl.WirelessCommandReply{}, nil
}

// Start starts the netctl server.
func (s *Server) Start() error {
	db, err := createDatabase()
	if err != nil {
		return fmt.Errorf("could not start netctl server: %v", err)
	}

	s.database = db

	go startDBusAdapter(s)

	return nil
}
