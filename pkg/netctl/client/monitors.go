// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"log"
	"time"

	"golang.org/x/net/context"

	"gitlab.com/redfield/netctl/api/netctl"
	"gitlab.com/redfield/netctl/pkg/netctl/wireless"
)

func (c *Client) startMonitors() {
	go c.monitorConnectionState()
	go c.monitorIPAddress()
	go c.monitorSignalStrength()
	go c.monitorAvailableNetworks()
	go c.monitorInterfaceStats()
}

func (c *Client) monitorConnectionState() {
	convertState := map[wireless.State]netctl.ConnProperties_State{
		wireless.Disconnected: netctl.ConnProperties_DISCONNECTED,
		wireless.Connected:    netctl.ConnProperties_CONNECTED,
		wireless.Connecting:   netctl.ConnProperties_CONNECTING,
	}

	// Assume that state begins as 'Disconnected'
	prevState := netctl.ConnProperties_DISCONNECTED

	for {
		time.Sleep(1 * time.Second)

		state := convertState[c.manager.State()]

		var portalURL string
		if state == netctl.ConnProperties_CONNECTED && prevState != netctl.ConnProperties_CONNECTED {
			// State changed to Connected -- check for portal
			if pres, url := c.manager.Interface().CheckForPortal(); pres {
				state = netctl.ConnProperties_PORTAL
				portalURL = url
			}
		}

		if state != prevState {
			c.updateConnectionState(state, portalURL)
		}

		// Keep track of the state
		prevState = state
	}
}

func (c *Client) updateConnectionState(state netctl.ConnProperties_State, url string) {
	c.mux.Lock()
	defer c.mux.Unlock()

	c.props.State = state
	c.props.PortalUrl = url

	u := netctl.StatusUpdate{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
		Type:   netctl.StatusUpdate_STATE,
		Props:  c.props,
	}

	r := netctl.UpdateStatusRequest{Update: &u}

	c.grpcClient.UpdateStatus(context.Background(), &r)
}

func (c *Client) monitorIPAddress() {
	// Get the IP address found during initialization
	c.mux.Lock()
	prevIP := c.props.GetIface().GetIpAddress()
	c.mux.Unlock()

	for {
		time.Sleep(1 * time.Second)

		if ip := c.manager.IPAddress(); ip != prevIP {
			c.updateIPAddress(ip)

			// Keep track of the new IP
			prevIP = ip
		}
	}
}

func (c *Client) updateIPAddress(ip string) {
	c.mux.Lock()
	defer c.mux.Unlock()

	c.props.Iface.IpAddress = ip
	c.props.WifiConf.Ssid = c.manager.CurrentSSID()

	u := netctl.StatusUpdate{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
		Type:   netctl.StatusUpdate_IP,
		Props:  c.props,
	}

	r := netctl.UpdateStatusRequest{Update: &u}

	c.grpcClient.UpdateStatus(context.Background(), &r)
}

func (c *Client) monitorSignalStrength() {
	// Get the signal strength found during initialization
	c.mux.Lock()
	prevSignal := c.props.GetIface().GetSignalStrength()
	c.mux.Unlock()

	for {
		time.Sleep(1 * time.Second)

		if signal := c.manager.SignalStrength(); signal != prevSignal {
			c.updateSignalStrength(signal)

			// Keep track of the new IP
			prevSignal = signal
		}
	}
}

func (c *Client) updateSignalStrength(signal string) {
	c.mux.Lock()
	defer c.mux.Unlock()

	c.props.Iface.SignalStrength = signal

	u := netctl.StatusUpdate{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
		Type:   netctl.StatusUpdate_SIGNAL,
		Props:  c.props,
	}

	r := netctl.UpdateStatusRequest{Update: &u}

	c.grpcClient.UpdateStatus(context.Background(), &r)
}

func (c *Client) monitorAvailableNetworks() {
	for {
		var aps []*netctl.AccessPoint

		for _, v := range c.manager.AvailableNetworks() {
			ap := netctl.AccessPoint{
				Ssid:           v.SSID,
				SignalStrength: v.SignalStrength.String(),
				KeyMgmt:        v.KeyManagement[0],
			}

			aps = append(aps, &ap)
		}

		c.updateAvailableNetworks(aps)

		time.Sleep(5 * time.Second)
	}
}

func (c *Client) updateAvailableNetworks(aps []*netctl.AccessPoint) {
	c.mux.Lock()
	defer c.mux.Unlock()

	c.props.Iface.AccessPoints = aps

	u := netctl.StatusUpdate{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
		Type:   netctl.StatusUpdate_ACCESSPOINTS,
		Props:  c.props,
	}

	r := netctl.UpdateStatusRequest{Update: &u}

	c.grpcClient.UpdateStatus(context.Background(), &r)
}

func (c *Client) monitorInterfaceStats() {
	for {
		stats, err := c.manager.Interface().Stats()
		if err != nil {
			log.Printf("could not get stats for interface '%v': %v", c.manager.Interface().Name, err)

			// If there was an error getting the stats, don't send an update
			time.Sleep(5 * time.Second)
			continue
		}

		c.updateInterfaceStats(stats)

		time.Sleep(5 * time.Second)
	}
}

func (c *Client) updateInterfaceStats(stats wireless.InterfaceStats) {
	c.mux.Lock()
	defer c.mux.Unlock()

	c.props.Iface.Stats = &netctl.NetworkInterface_Stats{
		RxBytes:   stats.RxBytes,
		RxErrors:  stats.RxErrors,
		RxDropped: stats.RxDropped,
		RxPackets: stats.RxPackets,
		TxBytes:   stats.TxBytes,
		TxErrors:  stats.TxErrors,
		TxDropped: stats.TxDropped,
		TxPackets: stats.TxPackets,
	}

	u := netctl.StatusUpdate{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
		Type:   netctl.StatusUpdate_STATS,
		Props:  c.props,
	}

	r := netctl.UpdateStatusRequest{Update: &u}

	c.grpcClient.UpdateStatus(context.Background(), &r)
}
