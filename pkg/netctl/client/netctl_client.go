// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"fmt"
	"log"
	"sync"
	"time"

	"golang.org/x/net/context"
	"google.golang.org/grpc"

	"gitlab.com/redfield/netctl/api/netctl"
	"gitlab.com/redfield/netctl/pkg/netctl/wireless"
)

// ClientOption is used to configure a netctl client.
type ClientOption interface {
	apply(*clientOptions)
}

type clientOptions struct {
	// Indicates if Client should try to connect to most-recently used
	// known network on startup. If there are none in present in the scan
	// results, nothing happens.
	doConnectLastNetwork bool

	// Indicates if the client should cache network configurations,
	// and re-use them on connect.
	doRememberNetworks bool

	// Indicates if the Client should perform periodic AP scans.
	doPeriodicScan bool
}

type funcClientOption struct {
	f func(*clientOptions)
}

func (fco *funcClientOption) apply(c *clientOptions) {
	fco.f(c)
}

func newFuncClientOption(f func(*clientOptions)) *funcClientOption {
	return &funcClientOption{f}
}

// WithConnectLastNetwork tells the Client to try to connect to the last
// known network on startup.
func WithConnectLastNetwork() ClientOption {
	return newFuncClientOption(func(c *clientOptions) {
		c.doConnectLastNetwork = true
	})
}

// WithRememberNetworks tells the Client to cache network configurations.
func WithRememberNetworks() ClientOption {
	return newFuncClientOption(func(c *clientOptions) {
		c.doRememberNetworks = true
	})
}

// WithPeriodicScan tells the client to periodically scan for wireless networks.
func WithPeriodicScan() ClientOption {
	return newFuncClientOption(func(c *clientOptions) {
		c.doPeriodicScan = true
	})
}

// Client represents a netctl client.
type Client struct {
	uuid string

	manager *wireless.Manager

	// Any property monitors started by the client will
	// modify props, and use the grpcClient to send status
	// updates. All of these operations should be done
	// with mutual exclusion.
	mux        sync.Mutex
	props      *netctl.ConnProperties
	grpcClient netctl.NetCtlServiceClient

	// Start up options for the client
	*clientOptions
}

// NewClient returns a new Client with an initialized netctl service client. Each client
// corresponds to a network interface.
func NewClient(conn *grpc.ClientConn, ifname string, opts ...ClientOption) (*Client, error) {
	gc := netctl.NewNetCtlServiceClient(conn)

	m, err := wireless.NewManager(ifname)
	if err != nil {
		return nil, fmt.Errorf("could not create new Client: %v", err)
	}

	// Construct 'uuid' vm name and interface name
	uuid := fmt.Sprintf("%v-%v", queryVMName(), ifname)

	c := &Client{
		uuid:          uuid,
		manager:       m,
		grpcClient:    gc,
		clientOptions: &clientOptions{},
	}

	// Initialize props
	iface := netctl.NetworkInterface{
		Type:           netctl.NetworkInterface_WIRELESS,
		Name:           c.manager.Interface().Name,
		IpAddress:      c.manager.IPAddress(),
		SignalStrength: c.manager.SignalStrength(),
	}

	props := netctl.ConnProperties{
		Iface:    &iface,
		State:    netctl.ConnProperties_DISCONNECTED,
		WifiConf: &netctl.WirelessNetworkConf{},
	}

	c.props = &props

	for _, opt := range opts {
		opt.apply(c.clientOptions)
	}

	return c, nil
}

// Register registers the Client with a netctl service. When the client is registered,
// it starts to monitor its wireless interface and begins listening for commands from
// the netctl service.
func (c *Client) Register() {
	r := netctl.RegisterClientRequest{
		Client: &netctl.NetCtlClient{Uuid: c.uuid},
	}

	c.grpcClient.RegisterClient(context.Background(), &r)

	c.start()
}

func (c *Client) start() {
	// Start the monitors
	c.startMonitors()

	// Check client options, and execute accordingly.
	go c.startScan()

	if c.doConnectLastNetwork {
		err := c.connectToLastNetwork()
		if err != nil {
			log.Print(err)
		}
	}

	// Finally, start the command handler
	go c.commandHandler()
}

func (c *Client) connectToLastNetwork() error {
	r := netctl.GetSavedNetworksRequest{Client: &netctl.NetCtlClient{Uuid: c.uuid}}

	saved, err := c.grpcClient.GetSavedNetworks(context.Background(), &r)
	if err != nil {
		return fmt.Errorf("could not get saved networks: %v", err)
	}

	// Scan may not be finished yet, wait a little bit for a populated list.
	avail := c.manager.AvailableNetworks()
	for start := time.Now(); time.Since(start) < 20*time.Second; {
		if len(avail) > 0 {
			break
		}

		avail = c.manager.AvailableNetworks()
	}

	// Go through the saved networks in order. If there is a match with the scan
	// results, connect to that network with the cached configuration.
	for _, s := range saved.GetNetworks() {
		ssid := s.GetSsid()
		for _, a := range avail {
			if ssid == a.SSID {
				return c.manager.Connect(formatNetworkConfiguration(s))
			}
		}
	}

	// No network found
	return nil
}

func (c *Client) startScan() {
	var scanInterval time.Duration

	// If periodic scan is disabled, return after first scan
	if !c.doPeriodicScan {
		if err := c.manager.Scan(); err != nil {
			log.Printf("unable to perform AP scan: %v", err)
		}

		return
	}

	for {
		if err := c.manager.Scan(); err != nil {
			log.Printf("unable to perform AP scan: %v", err)
		}

		// Scan more frequently when interface is disconnected
		if c.manager.State() == wireless.Disconnected {
			scanInterval = 20 * time.Second
		} else {
			scanInterval = 120 * time.Second
		}

		// Sleep until next scan
		time.Sleep(scanInterval)
	}
}
