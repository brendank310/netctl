// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package client

import (
	"log"

	"golang.org/x/net/context"

	"gitlab.com/redfield/netctl/api/netctl"
	"gitlab.com/redfield/netctl/pkg/netctl/supplicant"
)

func (c *Client) commandHandler() {
	r := netctl.MonitorCommandsRequest{Client: &netctl.NetCtlClient{Uuid: c.uuid}}

	stream, err := c.grpcClient.MonitorCommands(context.Background(), &r)
	if err != nil {
		log.Print(err)
		return
	}

	for {
		r, err := stream.Recv()

		if err != nil {
			log.Print(err)
			return
		}

		cmd := r.GetCmd()

		switch cmd.GetType() {

		case netctl.NetCtlCommand_CONNECT:
			err := c.manager.Connect(formatNetworkConfiguration(cmd.WifiConf))
			if err != nil {
				log.Printf("unable to connect to network: %v", err)
				continue
			}

			// Save the network configuration
			if c.doRememberNetworks {
				n := netctl.SaveNetworkRequest{
					Client:  &netctl.NetCtlClient{Uuid: c.uuid},
					Network: cmd.WifiConf,
				}

				c.grpcClient.SaveNetwork(context.Background(), &n)
			}

		case netctl.NetCtlCommand_DISCONNECT:
			if err := c.manager.Disconnect(); err != nil {
				log.Printf("unable to disconnect from network: %v", err)
			}

		default:
			log.Printf("received unknown command type: %v", cmd.GetType())
		}
	}
}

func formatNetworkConfiguration(conf *netctl.WirelessNetworkConf) supplicant.NetworkConfiguration {
	c := supplicant.NetworkConfiguration{
		SSID:          conf.Ssid,
		PSK:           conf.Psk,
		KeyManagement: conf.KeyMgmt,
		Identity:      conf.Identity,
		Password:      conf.Password,
		EAP:           conf.Eap,
		CaCert:        conf.CaCert,
		ClientCert:    conf.ClientCert,
		PrivKey:       conf.PrivKey,
		PrivKeyPasswd: conf.PrivKeyPasswd,
	}

	return c
}
