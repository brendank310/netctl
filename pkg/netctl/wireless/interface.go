// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"fmt"
	"log"
	"net"
	"strings"

	"github.com/prometheus/procfs"
)

// Interface represents a wireless network interface.
type Interface struct {
	Name string // Name of the network interface
}

// InterfaceStats represent the statistics of an interface
// found in /proc/net/dev.
type InterfaceStats struct {
	RxBytes   uint64
	RxDropped uint64
	RxErrors  uint64
	RxPackets uint64
	TxBytes   uint64
	TxDropped uint64
	TxErrors  uint64
	TxPackets uint64
}

// Stats returns the InterfaceStats associated with an Interface.
func (i *Interface) Stats() (InterfaceStats, error) {
	nd, err := procfs.NewNetDev()
	if err != nil {
		return InterfaceStats{}, err
	}

	if v, ok := nd[i.Name]; ok {
		stats := InterfaceStats{
			RxBytes:   v.RxBytes,
			RxDropped: v.RxDropped,
			RxErrors:  v.RxErrors,
			RxPackets: v.RxPackets,
			TxBytes:   v.TxBytes,
			TxDropped: v.TxDropped,
			TxErrors:  v.TxErrors,
			TxPackets: v.TxPackets,
		}

		return stats, nil
	}

	return InterfaceStats{}, fmt.Errorf("network interface %v not found in /proc/net/dev", i.Name)
}

// IPv4Addr returns the IPv4 address of the interface if it has one, and returns an error
// otherwise.
func (i *Interface) IPv4Addr() (net.IP, error) {
	iface, err := net.InterfaceByName(i.Name)
	if err != nil {
		return net.IP{}, err
	}

	addrs, err := iface.Addrs()
	if err != nil {
		return net.IP{}, err
	}

	// Look for valid IP address
	for _, v := range addrs {
		// Make sure the address is valid IPv4 in dotted decimal notation
		addr := strings.Split(v.String(), "/")[0]
		ip := net.ParseIP(addr).To4()

		if ip != nil {
			// Found a valid IPv4 address
			return ip, nil
		}

		log.Printf("Interface %v: Address %v is not a valid IPv4 address", i.Name, addr)
	}

	// Interface has no addresses
	return net.IP{}, fmt.Errorf("Interface %v has no valid addresses", i.Name)
}
