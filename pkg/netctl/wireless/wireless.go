// Copyright 2018 Assured Information Security, Inc. All Rights Reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package wireless

import (
	"gitlab.com/redfield/netctl/pkg/netctl/supplicant"
)

// State represents a connection state
type State int32

// Possible values of type State:
// 	Disconnected
// 	Connected
// 	Connecting
const (
	Disconnected State = iota
	Connected
	Connecting
)

// Manager is used to manage a wireless network interface.
type Manager struct {
	supplicant *supplicant.Supplicant

	iface *Interface

	config supplicant.NetworkConfiguration
}

// NewManager returns a Manager for a specified wireless
// interface.
func NewManager(ifname string) (*Manager, error) {
	s, err := supplicant.NewSupplicant(ifname)
	if err != nil {
		return nil, err
	}

	iface := Interface{
		Name: ifname,
	}

	m := Manager{
		supplicant: s,
		iface:      &iface,
		config:     supplicant.NetworkConfiguration{},
	}

	return &m, nil
}

// Interface returns the Interface associated with the Manager.
func (m *Manager) Interface() *Interface {
	return m.iface
}

// Connect connects to a network using a specified configuration.
func (m *Manager) Connect(conf supplicant.NetworkConfiguration) error {
	obj, err := m.supplicant.AddNetwork(conf)
	if err != nil {
		return err
	}

	err = m.supplicant.SelectNetwork(obj)
	if err != nil {
		return err
	}

	m.config = conf

	return nil
}

// Disconnect disconnects the current network.
func (m *Manager) Disconnect() error {
	if err := m.supplicant.NetworkDisconnect(); err != nil {
		return err
	}

	// Clear the network config
	m.config = supplicant.NetworkConfiguration{}

	return nil
}

// SignalStrength returns a description of the signal strength
// of the current network, e.g. "Very Good".
func (m *Manager) SignalStrength() string {
	signal := m.supplicant.CurrentSignalStrength()

	return signal.String()
}

// IPAddress returns a dotted decimal string representation of
// the Interface's IP address. If it has no IP, an empty string
// is returned.
func (m *Manager) IPAddress() string {
	ip, err := m.Interface().IPv4Addr()
	if err != nil {
		return ""
	}

	return ip.String()
}

// AvailableNetworks returns the scan results from a Manager's
// supplicant.
func (m *Manager) AvailableNetworks() []supplicant.ScanResult {
	return m.supplicant.ScanResults()
}

// CurrentSSID returns the SSID of the current network.
func (m *Manager) CurrentSSID() string {
	prop := m.supplicant.NetworkProperties()

	ssid, ok := prop["ssid"]
	if !ok {
		return ""
	}

	// Strip the quotes around the SSID that wpa_supplicant adds
	s := ssid.Value().(string)
	if len(s) > 0 {
		return s[1 : len(s)-1]
	}

	return ""
}

// State returns the current state of the Manager. Possible states are
// Disconnected, Connected, Connecting.
func (m *Manager) State() State {
	var state State

	// The manager state will be determined based on
	// the supplicant's interface state
	ws := m.supplicant.InterfaceState()

	switch ws {
	case "disconnected":
		state = Disconnected

	case "completed":
		// wpa state is 'Completed'. If the interface has a valid IP address,
		// return 'Connected'. Otherwise return 'Connecting'.
		if _, err := m.iface.IPv4Addr(); err != nil {
			state = Connecting
		} else {
			state = Connected
		}

	default:
		// wpa_supplicant goes through several states before ending
		// up at 'completed' -- simplify to 'connecting'
		state = Connecting
	}

	return state
}

// Scan triggers an AP scan with the Managers associated supplicant.
func (m *Manager) Scan() error {
	return m.supplicant.Scan()
}
